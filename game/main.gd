extends Node2D

var score = 0 		#змінна, в яку зберігається рахунок 
var died = false	#чи помер гравець
var started = false #чи гру розпочато першим натиском на екран
var touch_pressed = false
#Функція, яка винонується відразу, як вузол додано в сцену і він готовий до виконання
func _ready():
	new_game()	#Виклик функції яка ініціалізує нову гру

#Функція, яка додає визначену величину до рахунку, та викликає вивід рахунку на екран
func add_score():
	score += 1
	update_score()

#Вивід рахунку на екран
func update_score():
	$HUD/Score.text = str(score)
	$HUD/NinePatchRect/Score.text = str(score)

#Ініціалізація нової гри
func new_game():
	score = 0
	update_score()

#Функція, яка виконується в разі, коли гру програно
#Вона викликає анімацію, котра відображає панель з рахунком 
#та кнопку рестарту гри
#А також зупиняє всі анімації руху
func game_over():
	if died:
		return
	died = true
	$HUD/GameOverAnimation.play("GO")
	$Ground/AnimationPlayer.stop()
	$ObstacleSpawner.stop()
	$ObstacleSpawner.stop_movement()
	$BackGround/Forest1/AnimationPlayer.stop()
	$BackGround/Forest2/AnimationPlayer.stop()
	$BackGround/City01/AnimationPlayer.stop()
	$BackGround/Clouds01/AnimationPlayer.stop()


#Функція, яка опрацьовує всі події вводу, як то натискання клавіш
func _input(event):
	#стартувати появу перешкод, відразу після першого натиску на екран
	if (Input.is_action_just_pressed("flap") or event is InputEventScreenTouch) and !started:
		$ObstacleSpawner.start()
		started = true

#Опрацювання сигналу зіткнення гравця з землею
func _on_DZone_body_entered(body):
	if body.name == "Player":
		if body.has_method("die"):
			body.die()
			game_over()

#Опрацювання сигналу від клавіші рестарту гри
func _on_RestartButton_pressed():
	get_tree().reload_current_scene() #Перехавантаження сцени
	#для повного рестарту рівня

