extends RigidBody2D
#Змінна яка посилається на головний вузол сцени
onready var main_node = get_tree().get_root().get_child(0)
#Величина сили поштовху птахи вгору
export var FLAP_FORCE = -300
#Максимальний поворот гравця, при русі вгору
const MAX_ROTATION_DEGREES = -30.0
#Чи гру розпочато
var started = false
#Чи гравець живий
var alive = true
var screen_touch = false
var flapped = false
#Функція, котра виконується для кожного фізичного кадру гри

func _physics_process(delta):
	#Якщо в цьому кадрі надійшов сигнал про татисканню по екрану
	#і персонаж все ще живий
	var flap_input = flapped or Input.is_action_just_pressed("flap")
	flapped = false
	if  flap_input and alive:
		#Викликати ініціалізацію гравця, до початку
		start()
		#Викликати функцію взмаху крил
		flap()
	#Якщо поворот перевищує або рівний максимальному значенню
	#припинити повертати гравця
	if rotation_degrees <= MAX_ROTATION_DEGREES:
		angular_velocity = 0
		rotation_degrees = MAX_ROTATION_DEGREES
	#Якщо гравець рхається вниз
	#Повертати птага вниз до значення в 90 градусів
	if linear_velocity.y > 0:
		if rotation_degrees <= 90:
			angular_velocity = 3
		else:
			angular_velocity = 0
	flapped = false

func _input(event):
	#стартувати появу перешкод, відразу після першого натиску на екран
	if event is InputEventScreenTouch:
		flapped = true


#Функція, яка опрацьовує смерть гравця
func die():
	if !alive: 
		return
	alive = false
	$AnimationPlayer.stop()
	main_node.game_over()

#Функція, що ініціалізує гравця на початок гри
func start():
	#Якщо гру вже розпочато, повернутись з функції
	if started: 
		return
	started = true
	#Змінити значення маштабу гравітації, щоб розпочати падіння
	gravity_scale = 8.0
	#Почати відтворювати анімацію польоту
	$AnimationPlayer.play("flap")

#Функції, що виконується для взмаху крил
#та надає поштовх вгору
func flap():
	linear_velocity.y = FLAP_FORCE
	angular_velocity = -25.0
